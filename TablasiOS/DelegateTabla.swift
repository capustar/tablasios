//
//  DelegateTabla.swift
//  TablasiOS
//
//  Created by Marcelo on 21/11/17.
//  Copyright © 2017 EPS. All rights reserved.
//

import UIKit

class DelegateTabla : NSObject, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if let celda = tableView.cellForRow(at: indexPath)
        {
            //Si no hay marca de verificación la ponemos. Si la hay la quitamos
            if celda.textLabel?.textColor == UIColor.black
            {
                celda.textLabel?.textColor = UIColor.red
            }
            else
            {
                celda.textLabel?.textColor = UIColor.black
            }
            //deseleccionamos la celda, si no se quedará con el fondo gris
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
}

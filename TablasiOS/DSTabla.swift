//
//  DSTabla.swift
//  TablasiOS
//
//  Created by mastermoviles on 21/11/17.
//  Copyright © 2017 EPS. All rights reserved.
//

import UIKit

class DSTabla : NSObject, UITableViewDataSource
{
    @IBOutlet weak var edit_text_nuevo_elemento: UITextField!
    @IBOutlet weak var tabla: UITableView!
    
    //en la clase DSTabla, definimos e inicializamos la propiedad
    var lista = ["Karl Marx", "Rosa Luxemburgo","Lenin", "Alexandra Kollontai", "Clara Zetkin", "Friedrich Engels"]
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        let num_personas_lista = lista.count
        
        return num_personas_lista
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let nuevaCelda = tableView.dequeueReusableCell(withIdentifier: "celdaReutilizable",
                                                       for: indexPath)
        
        nuevaCelda.textLabel?.text = lista[indexPath.row]
        
        return nuevaCelda
    }
    
    func insertarCelda(enTabla : UITableView , enFila : Int, conTexto : String)
    {
        self.lista.insert(conTexto, at: enFila)
        let indexPath = IndexPath(row : enFila, section : 0)
        enTabla.insertRows(at: [indexPath], with: UITableViewRowAnimation.fade)
    }
    
    @IBAction func insertarElementoLista(_ sender: UIButton)
    {
        let texto_nuevo_elemento = self.edit_text_nuevo_elemento.text
        
        if (texto_nuevo_elemento != "")
        {
            let num_elementos_lista = lista.count
            
            insertarCelda(enTabla: self.tabla, enFila: num_elementos_lista, conTexto: texto_nuevo_elemento!)

            self.edit_text_nuevo_elemento.text = ""
        }
    }
}
